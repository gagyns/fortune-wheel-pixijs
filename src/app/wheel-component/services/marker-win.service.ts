import {ContainerService} from "./container.service";
import {PieService} from "./pie.service";
import {SpriteService} from "./sprite.service";

export class MarkerWinService {
  private markerContainer!: ContainerService;
  private pieMarker!: PieService;
  private arrowSpriteContainer!: ContainerService;
  private width!: number;
  private pieAngleDegree!: number;

  createMarker(width: number, pieAngleDegree: number) {
    this.width = width;
    this.pieAngleDegree = pieAngleDegree;
    this.markerContainer = new ContainerService();
    this.markerContainer.moveTop(this.width / 2);
    this.markerContainer.moveRight(this.width / 2);
    this.markerContainer.rotationDegree(this.pieAngleDegree / 2 );
    this.pieMarker = new PieService();
    this.pieMarker.setPieArguments(((this.width / 2) * 0.94), ['#1db67b'],  pieAngleDegree, 0);
    this.pieMarker.setLineParams(20, 0x091973, 1);
    this.pieMarker.create();
    this.markerContainer.addToContainer(this.pieMarker)
    this.appendArrow();
    return this.markerContainer
  }
  appendArrow() {
    this.arrowSpriteContainer = new ContainerService();
    this.arrowSpriteContainer.moveTop(-140);
    this.arrowSpriteContainer.moveRight(415);
    this.arrowSpriteContainer.rotationDegree(-10);
    const arrowSprite = new SpriteService('./assets/markerArrow.png',
      80, 120, 1,true, true);
    this.arrowSpriteContainer.addToContainer(arrowSprite);
    this.markerContainer.addToContainer(this.arrowSpriteContainer)
  }
}
