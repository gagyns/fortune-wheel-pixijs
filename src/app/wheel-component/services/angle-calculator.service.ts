import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AngleCalculatorService {
  private dataForWheel!: dataForWheel[]
  private pieAngleDegree!: number
  private itemID: number = 0;
  private numberOfTurns: number = 1;
  private animationAngle: number = 0;

  setDataForWheel(data: dataForWheel[]) {
    this.dataForWheel = data;
    this.pieAngleDegree = 360 / data.length;
    return this.pieAngleDegree;
  }

  getAngleForID() {
    const exacleAngleOfElement = 360 -(((this.itemID) * this.pieAngleDegree) - this.pieAngleDegree);
    this.animationAngle = exacleAngleOfElement + (360 * this.numberOfTurns)
    return this.animationAngle;
  }
  setWinItem() {
    this.itemID = Math.max(Math.round(Math.random() * (this.dataForWheel.length -1)), 1);
    return  this.dataForWheel[this.itemID-1].amount;
  }
}

export interface dataForWheel {
  "id": number,
  "color": string,
  "amount": string,
  "icon": string
}
