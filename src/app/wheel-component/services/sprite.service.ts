import * as PIXI from 'pixi.js';

export class SpriteService extends PIXI.Sprite {
  private imgPath!: string;
  private oldTexture!: PIXI.Texture;
  private ticker!: PIXI.Ticker;

  constructor(
    imgPath: string,
    width: number,
    height: number,
    alpha?: number,
    interactive?: boolean,
    buttonMode?: boolean
  ) {
    super();
    this.imgPath = imgPath;
    this.texture = PIXI.Texture.from(imgPath)
    this.width = width;
    this.height = height;
    this.alpha = alpha || 1;
    this.interactive = interactive || false;
    this.buttonMode = buttonMode || false;
  }
  setTexture(textrure: PIXI.Texture) {
    this.texture = textrure;
  }
  addEvent(event: string, collback: any) {
    this.on(event, collback)
  }
  IntervalTexture(oldTexture: string, texture: string, time: number) {
    this.oldTexture = PIXI.Texture.from(oldTexture);
    this.texture = PIXI.Texture.from(texture);
    setTimeout(() => {
      this.texture = this.oldTexture;
    }, 2000)
  }
  alfaTransition() {
    this.ticker = PIXI.Ticker.shared;
    let fadeOut = true
    let alpha = 1
    this.ticker.add(delta => {
      if(fadeOut) {
        alpha -= 0.01;
        if(this.alpha <= 0.01) { fadeOut = false }
        this.alpha = alpha;
      }
      if (!fadeOut) {
        alpha += 0.01;
        if(this.alpha >= 0.98) { fadeOut = true }
        this.alpha = alpha;
      }
    })
    this.ticker.start();
  }
}
