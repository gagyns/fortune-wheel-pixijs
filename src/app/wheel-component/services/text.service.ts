import * as PIXI from 'pixi.js';

export class TextService extends PIXI.Text {
  constructor(
    text: string,
    style: undefined,
    x?:number,
    y?: number
  ) {
    super(text, style);
    this.x = x || 0;
    this.y = y || 0;
  }
  override style = new PIXI.TextStyle(
{
        fontFamily: 'Arial',
        fontSize: 50,
        fontStyle: 'normal',
        fontWeight: 'bold',
        fill: ['#00ff99'], // gradient
      }
  )

  moveRight(value: number) {
    this.x = value;
  }

  moveTop(value: number) {
    this.y = value;
  }
}
