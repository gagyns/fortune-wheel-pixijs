import * as PIXI from 'pixi.js';

export class AnimatedSpriteService extends PIXI.AnimatedSprite {
  constructor(imagesArray: string[], time: number, width: number, height: number, alfa: number) {
    super(imagesArray.map(image => PIXI.Texture.from(image)));
    this.animationSpeed = time;
    this.width = width;
    this.height = height;
    this.alpha = alfa;
    this.autoUpdate = true;
    this.interactive = true;
    this.buttonMode = true;
    this.play();
  }
  updateImg(){
    // this.update=true;
  }
  addEvent(event: string, collback: any) {
    this.on(event, collback)
  }
  setTexture(textrure: PIXI.Texture) {
    this.texture = textrure;
  }
}
