import * as PIXI from 'pixi.js';

export class CircleService extends PIXI.Graphics {
  private lineStyleWidth: number = 0;
  private lineStyleColor: number = 0xFFFFFF;
  private lineStyleAlfa: number = 1;
  private fillColor: any = 0x1db67b;
  private fillAlfa: number = 1;
  private circleX: number = 0;
  private circleY: number = 0;
  private radius: number = 10;
  private circleAlfa: number = 1

  constructor() {
    super();
  }
  circleLineParams(width?:number , color?: number, alfa?: number) {
    this.lineStyleWidth = width || 0;
    this.lineStyleColor = color || 0xFFFFFF;
    this.lineStyleAlfa = alfa || 1;
    this.lineStyle(this.lineStyleWidth, this.lineStyleColor, this.lineStyleAlfa);
    this.drawCircle(this.circleX, this.circleY, this.radius);
    return this;
  }

  create(radius: number, fillColor: any, x?: number, y?: number, alfa?: number) {
    this.parseParams(arguments);
    this.beginFill(this.fillColor, this.fillAlfa);
    this.drawCircle(this.circleX, this.circleY, this.radius);
    this.endFill();
    return this;
  }
  parseParams(circleParams: any) {
    this.radius = circleParams[0];
    this.fillColor = circleParams[1];
    this.circleX = circleParams[2] || 0;
    this.circleY = circleParams[3] || 0;
    this.circleAlfa = circleParams[4] || 1;
  }
  lightUpBulb(color: number, index:number) {
    this.beginFill(color, this.fillAlfa);
    this.drawCircle(this.circleX, this.circleY, this.radius);
    this.endFill();
    setTimeout(() => {
      this.beginFill(this.fillColor, this.fillAlfa);
      this.drawCircle(this.circleX, this.circleY, this.radius);
      this.endFill();
    }, 2000)
  }
}
