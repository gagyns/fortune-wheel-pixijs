import * as PIXI from 'pixi.js';

export class ContainerService extends PIXI.Container{
  // private container!: PIXI.Container;

  constructor(left?: number, top?: number) {
    super();
    this.x = left || 0;
    this.y = top || 0;
  }

  moveRight(value: number) {
    this.x = value;
  }

  moveTop(value: number) {
    this.y = value;
  }

  addToContainer(element: any) {
    this.addChild(element);
  }
  rotationDegree(degree: number) {
    this.rotation = degree * (Math.PI / 180)
  }
}
