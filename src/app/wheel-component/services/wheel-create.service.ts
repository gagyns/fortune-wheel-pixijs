import {ContainerService} from "./container.service";
import {PieService} from "./pie.service";
import * as PIXI from 'pixi.js';
import {TextService} from "./text.service";
import {SpriteService} from "./sprite.service";

export class WheelCreateService {
  private wheelData!: any;
  private canvas!: { width: number; height: number };
  private wheelContainer!: ContainerService;
  private pieContainer!: ContainerService;
  private pieAngleDegree!: number;
  private pie!: PieService;
  private markerContainer!: ContainerService;
  private pieMarker!: PieService;
  private blurFilter!: PIXI.filters.BlurFilter;

  constructor(wheelData: any, canvas: {width: number, height: number}, pieAngleDegree: number) {
    this.wheelData = wheelData;
    this.canvas = canvas;
    this.pieAngleDegree = pieAngleDegree;
  }

  create() {
    this.wheelContainer = new ContainerService();
    this.wheelContainer.moveTop(this.canvas.width / 2);
    this.wheelContainer.moveRight(this.canvas.width / 2);

    this.wheelData.forEach((pieData: DataWheel, i:number) => {
      this.pieContainer = new ContainerService();
      this.pieContainer.rotationDegree((this.pieAngleDegree * i))

      const pieGeometryContainer = new ContainerService();
      pieGeometryContainer.rotationDegree(this.pieAngleDegree / 2 );
      this.pie = new PieService()
      this.pie.setPieArguments(((this.canvas.width / 2) * 0.9), [...pieData.color],  this.pieAngleDegree, 1);
      this.pie.create();

      const textContainer = new ContainerService();
      textContainer
        .addToContainer(new TextService(`${pieData.amount}`, undefined, 160, -26));

      const criptoImageContainer = new ContainerService(((this.canvas.width / 2) * 0.7), -40);
      const criptoSprite = new SpriteService(`./assets/cripto/${pieData.icon}.png`, 90, 90);
      criptoImageContainer.addToContainer(criptoSprite);

      pieGeometryContainer.addToContainer(this.pie);
      this.pieContainer.addToContainer(pieGeometryContainer);
      this.pieContainer.addToContainer(textContainer);
      this.pieContainer.addToContainer(criptoImageContainer);
      this.wheelContainer.addToContainer(this.pieContainer);
    })
    this.blurFilter = new PIXI.filters.BlurFilter(0, 8);

    this.wheelContainer.filters = [this.blurFilter];
  }
  returnWheelContainer() { return this.wheelContainer }
  rotateWheelDegree(angle: number) {
    this.wheelContainer.rotationDegree(angle);
  }

  setBlurFilter(blurStrength: number) {
    this.blurFilter.blur = blurStrength;
  }
}

interface DataWheel {
  "id": number;
  "color": string;
  "amount": string;
  "icon": string;
}
