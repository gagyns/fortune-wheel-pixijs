import * as PIXI from 'pixi.js';
import {ContainerService} from "../container.service";
import {CircleService} from "../circle.service";
import {SpriteService} from "../sprite.service";

class DataWheel {
}

export class BackgroundRingService {
  private backgroundRing!: ContainerService;
  private canvas: any;
  private wheelData: any;
  private lightContainer!: ContainerService;
  private pieAngleDegree!: number;
  private lightContainerRotate!: ContainerService;
  private lightsCollection: SpriteService[] = [];
  private lightObject!: SpriteService;

  constructor(canvas: any, wheelData: DataWheel, pieAngleDegree: number) {
    this.canvas = canvas;
    this.wheelData = wheelData;
    this.pieAngleDegree = pieAngleDegree;
  }
  createRing() {
    this.backgroundRing = new ContainerService();
    this.backgroundRing.moveTop(this.canvas.width / 2);
    this.backgroundRing.moveRight(this.canvas.width / 2);
    this.backgroundRing.addToContainer(new CircleService()
      .create((this.canvas.width / 2) * 0.95, 0xFF6CA0)
      .circleLineParams(13, 0xFF2B77, 1)
    )
    // black outline on Background
    this.backgroundRing.addToContainer(new CircleService()
      .create((this.canvas.width / 2) * 0.92, 0x00000)
    )
    this.createLights();
    this.animateLightUp()
    return this.backgroundRing;
  }
  createLights() {
    this.wheelData.forEach((pieData: DataWheel, i:number) => {
      this.lightContainerRotate = new ContainerService();
      this.lightContainer = new ContainerService();
      this.lightContainer.moveRight((this.canvas.width / 2) * 0.92);
      this.lightObject = new SpriteService('./assets/light_off.png',
        20, 20, 1,true, true);
      this.lightsCollection.push(this.lightObject);
      this.lightContainer.addToContainer(this.lightObject)
      this.lightContainerRotate.addToContainer(this.lightContainer);
      this.lightContainerRotate.rotationDegree((this.pieAngleDegree * i) - this.pieAngleDegree / 2)
      this.backgroundRing.addToContainer(this.lightContainerRotate);
    })
  }
  animateLightUp() {
    setInterval(() => {
      const lightSelect1 = Math.floor(Math.random() * this.wheelData.length);
      this.lightsCollection[lightSelect1]
        .IntervalTexture('./assets/light_off.png', './assets/light_on.png', 2000);
      const lightSelect2 = Math.floor(Math.random() * this.wheelData.length);
      this.lightsCollection[lightSelect2]
        .IntervalTexture('./assets/light_off.png', './assets/light_on.png', 2000);
    }, 500)
  }
}
