import * as PIXI from 'pixi.js';

export class PieService extends PIXI.Graphics  {

  constructor() {
    super();
    this.x = 2500;
    this.x = 2500;
  }

  private gradientFill!: boolean;
  private stroke:number = 0;
  private strokeColor:number = 0xffffff;
  private fillColor:number = 0xCCE30B;
  private startColor:string = '0xCCE30B';
  private reflectColor:string = '0xCCE30B';
  private endColor:string = '0xCCE30B';
  private fillAlfa:number | undefined = 1;
  private endAngle:number = 22;
  private startAngle:number = 0;
  private radius:number = 2000;
  private yRadius:number = this.radius;
  private lineStyleAlfa:number = 1;
  private ctx!: CanvasRenderingContext2D | null;

  parseHexColor(fillColor: string[]) {
    this.gradientFill = fillColor.length > 1;
    if (!this.gradientFill) {
      this.fillColor = parseInt(`0x${fillColor[0].substring(1)}`)
    } else {
      this.startColor = fillColor[0];
      this.reflectColor = fillColor[1];
      this.endColor = fillColor[2];
    }
  }
  setPieArguments(radius:number, fillColor:string[], arc:number, alfa?:number, startAngle?:number, x?:number, y?:number) {
    this.parseHexColor(fillColor)
    this.fillAlfa = alfa;
    this.radius = radius;
    this.yRadius = radius;
    this.endAngle = arc;
    this.startAngle = startAngle || 0;
    this.x = x || 0;
    this.y = y || 0;
  }
  setLineParams(width?:number , color?: number, alfa?: number) {
    this.stroke = width || 0; this.strokeColor = color || 0x000000; this.lineStyleAlfa = alfa || 1;
  }

  create() {
    if (this.gradientFill) {
      let matrix = new PIXI.Matrix(1.6,0, 0.6, 2, 0, 10);
      this.beginTextureFill({texture: this.gradient(this.startColor, this.reflectColor, this.endColor), matrix: matrix, alpha: this.fillAlfa})
    } else {
      this.beginFill(this.fillColor, this.fillAlfa);
    }
    this.lineStyle(this.stroke, this.strokeColor, this.lineStyleAlfa);
    this.constructPie();
  }

  gradient(from: string, reflectColor: string, to: string): any {
    const c = document.createElement("canvas");
    this.ctx = c.getContext("2d");
    // @ts-ignore
    const grd = this.ctx.createLinearGradient(0,0,500,100);
    grd.addColorStop(0, to);
    grd.addColorStop(0.5, from);
    grd.addColorStop(0.55, reflectColor);
    grd.addColorStop(0.7, from);
    grd.addColorStop(1, to);
    // @ts-ignore
    this.ctx.fillStyle = grd;
    // @ts-ignore
    this.ctx.fillRect(0,0,500,200);
    // @ts-ignore
    return new PIXI.Texture.from(c);
  }

  constructPie() {
    let segs = Math.ceil(Math.abs(this.endAngle) / 45);
    let segAngle = this.endAngle / segs;
    let theta = -(segAngle / 180) * Math.PI;
    let angle = -(this.startAngle / 180) * Math.PI;
    let ax = this.x + Math.cos(this.startAngle / 180 * Math.PI) * this.radius;
    let ay = this.y + Math.sin(-this.startAngle / 180 * Math.PI) * this.yRadius;
    let angleMid, bx, by, cx, cy;

    if (this.yRadius === 0)
      this.yRadius = this.radius;

    this.moveTo(this.x, this.y);
    this.lineTo(ax, ay);

    for (let i = 0; i < segs; ++i) {
      angle += theta;
      angleMid = angle - (theta / 2);
      bx = this.x + Math.cos(angle) * this.radius;
      by = this.y + Math.sin(angle) * this.yRadius;
      cx = this.x + Math.cos(angleMid) * (this.radius / Math.cos(theta / 2));
      cy = this.y + Math.sin(angleMid) * (this.yRadius / Math.cos(theta / 2));
      this.quadraticCurveTo(cx, cy, bx, by);
    }
    this.lineTo(this.x, this.y);
  }
}
