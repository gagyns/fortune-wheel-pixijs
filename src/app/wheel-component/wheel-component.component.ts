import * as PIXI from 'pixi.js';
import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ContainerService} from "./services/container.service";
import {AngleCalculatorService} from "./services/angle-calculator.service";
import {WheelCreateService} from "./services/wheel-create.service";
import {BackgroundRingService} from "./services/backgroundRing/backgroundRing.service";
import {SpriteService} from "./services/sprite.service";
import {MarkerWinService} from "./services/marker-win.service";

@Component({
  selector: 'app-wheel-component',
  templateUrl: './wheel-component.component.html',
  styleUrls: ['./wheel-component.component.scss'],
  providers:  [ AngleCalculatorService ]
})
export class WheelComponentComponent implements OnInit {
  @ViewChild('canvas', {static: true}) codeChangeTrackerContainer!: ElementRef;

  private canvas = {width: 1000, height: 1000}
  private app!: PIXI.Application;
  private pieAngleDegree: number = 0;
  private appWidth!: number;
  private appHeight!: number;
  private layerBackGround!: ContainerService;
  private layer0!: ContainerService;
  private layer1!: ContainerService;
  private wheelData: any;
  private backgroundRing!: ContainerService;
  private logoContainer!: ContainerService;
  private wheelContainerService!: WheelCreateService;
  private ticker!: PIXI.Ticker;
  public winValue!: string;
  private markerContainer!: ContainerService;
  private magentaContainer!: ContainerService;
  private blueStrength: number = 8;


  constructor(
    private ngZone: NgZone,
    private http: HttpClient,
    private angleCalculatorService: AngleCalculatorService
  ) { }

  ngOnInit() {
    this.appWidth = this.codeChangeTrackerContainer.nativeElement.offsetWidth;
    this.appHeight = this.codeChangeTrackerContainer.nativeElement.offsetHeight;

    this.http.get('./assets/wheel-setup.json',  { withCredentials: true })
      .subscribe((data: any) => {
        this.wheelData = data;
        this.pieAngleDegree = this.angleCalculatorService.setDataForWheel(data);
        this.drawLayers()
        this.draw();
      });
  }
  drawLayers() {
    this.layerBackGround = new ContainerService();
    this.layer0 = new ContainerService();
    this.layer1 = new ContainerService();
  }

  draw(): void {
    this.ngZone.runOutsideAngular(() => {
      this.app = new PIXI.Application({
        width: this.canvas.width,
        height: this.canvas.height,
        backgroundColor: 0x000000,
      });

      // background ring
      this.layerBackGround
        .addToContainer(new BackgroundRingService(this.canvas, this.wheelData, this.pieAngleDegree)
          .createRing());

      this.wheelContainerService = new WheelCreateService(this.wheelData, this.canvas, this.pieAngleDegree);
      this.wheelContainerService.create();
      this.markerContainer = new MarkerWinService().createMarker(this.canvas.width, this.pieAngleDegree);

      // =========================== SPRITE ============================= //
      this.logoContainer = new ContainerService();
      this.logoContainer.moveTop(this.canvas.width / 2 - (this.canvas.width * 0.15));
      this.logoContainer.moveRight(this.canvas.width / 2  - (this.canvas.width * 0.15));
      let textureAdmiral = PIXI.Texture.from('./assets/admiralLogo_alfa.png');
      let textureSpin = PIXI.Texture.from('./assets/admiralLogo_spin_alfa.png');

      const logoSpriteSpin = new SpriteService('./assets/admiralLogo_spin_alfa.png',
        this.canvas.width * 0.3, this.canvas.width * 0.3, 1,true, true);
      const logoSprite = new SpriteService('./assets/admiralLogo_alfa.png',
        this.canvas.width * 0.3, this.canvas.width * 0.3, 1,true, true);

      logoSprite.alfaTransition()

      this.logoContainer.addToContainer(logoSpriteSpin)
      this.logoContainer.addToContainer(logoSprite)
      logoSprite.addEvent('touchstart', () => this.rotateWheel());
      logoSprite.addEvent('click', () => this.rotateWheel());
      logoSprite.addEvent('mouseover', () => logoSprite.setTexture(textureSpin));
      logoSprite.addEvent('mouseout', () => logoSprite.setTexture(textureAdmiral));
      logoSprite.addEvent('mouseout', () => logoSprite.alpha = 1);

      // =========================== SPRITE ================================ //
      // this.magentaContainer = new ContainerService();
      // this.magentaContainer.moveTop((this.canvas.width * 0.022));
      // this.magentaContainer.moveRight((this.canvas.width * 0.022));
      // const MagentaSprite = new SpriteService('./assets/gradients/radial_alfa_darker.png',
      //   this.canvas.width * 0.96, this.canvas.width * 0.96, 0.7);
      // this.magentaContainer.addToContainer(MagentaSprite);

      // ===================================================================== //
      this.layer0.addToContainer(this.wheelContainerService.returnWheelContainer());

      this.layer1.addToContainer(this.markerContainer);
      this.layer1.addToContainer(this.logoContainer);
      // this.layer1.addToContainer(this.magentaContainer);

      this.app.stage.interactive = true;

      this.app.stage.addChild(this.layerBackGround);
      this.app.stage.addChild(this.layer0);
      this.app.stage.addChild(this.layer1);
    });
    this.app.view.className = 'canvas';
    this.codeChangeTrackerContainer.nativeElement.appendChild(this.app.view);

  }

  rotateWheel() {
    this.winValue = this.angleCalculatorService.setWinItem();
    const angleOfItem = this.angleCalculatorService.getAngleForID()
    let angle = 0;
      this.ticker = PIXI.Ticker.shared;
      this.ticker.add(delta => {
          if (angle < angleOfItem ) {
            angle += (1.4 - (angle / angleOfItem));
            const blueStrength = ((Math.round(((1.4 - (angle / angleOfItem)) * 100)) / 100) - 0.4) * this.blueStrength;
            this.wheelContainerService.rotateWheelDegree(angle);
            this.wheelContainerService.setBlurFilter(blueStrength);
          } else {
            this.ticker.destroy();
          }
        })
      this.ticker.start();
  }
}

